import 'phaser-ce';

const GAME_WIDTH = 800;
const GAME_HEIGHT = 600;
const TEXT_FONT = "65px Arial";
const TEXT_COLOR = "#000000";
const TEXT_ALIGNMENT = "center";

const game = new Phaser.Game(
    GAME_WIDTH,
    GAME_HEIGHT,
    Phaser.AUTO,
    '',
    {
        preload: preload,
        create: create
    });

function preload() {
    game.load.image('ufo', '/src/assets/ufo.png');
}

let cont = 0
let contador
let text
let botonU
let textU
let botonD
let textD
let sprite
let tween
let w;
let h;

function create() {

    //CONTADOR
    contador = game.add.graphics(game.world.centerX - 200, game.world.centerY - 82);
    pintaContador(contador, 0xFFFFFF);
    text = game.add.text(
        game.world.centerX + 25,
        game.world.centerY,
        "" + cont + "",
        {
            font: TEXT_FONT,
            fill: TEXT_COLOR,
            align: TEXT_ALIGNMENT,
        });

    //BOTÓN SUMAR
    botonU = game.add.graphics(game.world.centerX - 200, game.world.centerY + 50);
    pintaCuadros(botonU, 0xFFFFFF);
    botonU.inputEnabled = true;
    botonU.input.useHandCursor = true;
    botonU.events.onInputDown.add(up, this);
    textU = game.add.text(
        game.world.centerX - 166,
        game.world.centerY + 66,
        "+",
        {
            font: TEXT_FONT,
            fill: TEXT_COLOR,
            align: TEXT_ALIGNMENT,
        });

    //BOTÓN RESTAR
    botonD = game.add.graphics(game.world.centerX + 100, game.world.centerY + 50);
    pintaCuadros(botonD, 0xFFFFFF);
    botonD.inputEnabled = true;
    botonD.input.useHandCursor = true;
    botonD.events.onInputDown.add(down, this);
    textD = game.add.text(
        game.world.centerX + 140,
        game.world.centerY + 60,
        "-",
        {
            font: TEXT_FONT,
            fill: TEXT_COLOR,
            align: TEXT_ALIGNMENT,
        });

    game.stage.backgroundColor = '#5e5e5e';
    text.anchor.setTo(1, 1);

    //FRAME
    sprite = game.add.sprite(350, 0, 'ufo');
    sprite.scale.set(3)
    w = game.width - sprite.width;
    h = game.height - sprite.height;
}

function update() {

}

function pintaContador(boton, color) {
    boton.clear();
    boton.beginFill(color);
    boton.lineStyle(1, 0x000000, 1);
    boton.lineTo(100, 0);
    boton.lineTo(400, 0);
    boton.lineTo(400, 100);
    boton.lineTo(0, 100);
    boton.endFill();
}

function pintaCuadros(boton, color) {
    boton.clear();
    boton.beginFill(color);
    boton.lineStyle(1, 0x000000, 1);
    boton.lineTo(100, 0);
    boton.lineTo(100, 100);
    boton.lineTo(0, 100);
    boton.lineTo(0, 0);
    boton.endFill();
}

function up() {
    if (cont === 10) {
        text.addColor("#ff0000", 0);
        pintaCuadros(botonU, 0xC0C0C0)
        botonU.inputEnabled = false;
        botonU.input.useHandCursor = true;
    } else {
        cont++;
        text.setText("" + cont + "");
        text.addColor("#000000", 0)
        pintaCuadros(botonU, 0xFFFFFF)
        botonU.inputEnabled = true;
        botonU.input.useHandCursor = true;
    }
    tween = game.add.tween(sprite).to({x: [w, w, 0, 0], y: [0, h, h, 0]}, 5000, "Sine.easeInOut", true, -1, false);
}

function down() {
    if (cont > 0) {
        cont--;
        text.setText("" + cont + "");
        text.addColor("#000000", 0)
        pintaCuadros(botonU, 0xFFFFFF)
        botonU.inputEnabled = true;
        botonU.input.useHandCursor = true;
    }
    tween = game.add.tween(sprite).to({x: [0, 0, w, w], y: [0, h, h, 0]}, 5000, "Sine.easeInOut", true, -1, false);
}

